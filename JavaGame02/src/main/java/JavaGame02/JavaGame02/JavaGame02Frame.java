package JavaGame02.JavaGame02;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import metods.MetodoTeclado;
import metods.botonesEnable;
import metods.desactivarActivar;
import metods.pistas;
import metods.reinicioPartida;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.LineBorder;

public class JavaGame02Frame extends JFrame {

	private JPanel contentPane;
	private JMenuBar barraMenu;
	private static JLabel lblNewLabel1;
	private static JLabel lblNewLabel2;
	private static JLabel lblNewLabel3;
	private static JLabel lblNewLabel4;
	private static JLabel lblNewLabel5;
	private static int contadorFallo = 5;
	private static int contadorAcierto = 0;
	private JLabel lblNewLabel = new JLabel("Vidas");
	private JLabel lblNewLabelVidas;
	private static JPanel cabeza;
	private static JPanel Cuerpo;
	private static JPanel brazoIzquierdo;
	private static JPanel brazoDerecho;
	private static JPanel piernaIzquierda;
	private static JPanel piernaDerecha;
	private static JPanel vida1;
	private static JPanel vida2;
	private static JPanel vida3;
	private static JPanel vida4;
	private static JPanel vida5;
	public static int numeroAleatorio = (int) (Math.random()*5);
	private static String[] palabrasPosibles = new String[] {"larva", "coche", "arabe", "arena", "carta"};
	private static String palabraPosibleRandom = palabrasPosibles[numeroAleatorio];
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JavaGame02Frame frame = new JavaGame02Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JavaGame02Frame() {
		numeroAleatorio = (int) (Math.random()*5);
		palabrasPosibles = new String[] {"larva", "coche", "arabe", "arena", "carta"};
		palabraPosibleRandom = palabrasPosibles[numeroAleatorio];
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 713, 565);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		vida1 = new JPanel();
		vida1.setBorder(null);
		vida1.setBackground(Color.RED);
		vida1.setForeground(Color.RED);
		vida1.setBounds(570, 37, 50, 46);
		contentPane.add(vida1);
		
		vida2 = new JPanel();
		vida2.setBackground(Color.RED);
		vida2.setBorder(null);
		vida2.setBounds(570, 85, 50, 46);
		contentPane.add(vida2);
		
		vida3 = new JPanel();
		vida3.setBackground(Color.RED);
		vida3.setBorder(null);
		vida3.setBounds(570, 134, 50, 46);
		contentPane.add(vida3);
		
		vida4 = new JPanel();
		vida4.setBackground(Color.RED);
		vida4.setBorder(null);
		vida4.setBounds(570, 183, 50, 46);
		contentPane.add(vida4);
		
		vida5 = new JPanel();
		vida5.setBackground(Color.RED);
		vida5.setBorder(null);
		vida5.setBounds(570, 230, 50, 46);
		contentPane.add(vida5);
		

		
		cabeza = new JPanel();
		cabeza.setBounds(34, 37, 82, 81);
		contentPane.add(cabeza);
		
		Cuerpo = new JPanel();
		Cuerpo.setBounds(64, 134, 20, 221);
		contentPane.add(Cuerpo);
		
		brazoIzquierdo = new JPanel();
		brazoIzquierdo.setBounds(29, 146, 17, 81);
		contentPane.add(brazoIzquierdo);
		
		brazoDerecho = new JPanel();
		brazoDerecho.setBounds(99, 146, 17, 81);
		contentPane.add(brazoDerecho);
		
		piernaIzquierda = new JPanel();
		piernaIzquierda.setBounds(34, 335, 17, 81);
		contentPane.add(piernaIzquierda);
		
		piernaDerecha = new JPanel();
		piernaDerecha.setBounds(99, 335, 17, 81);
		contentPane.add(piernaDerecha);
		
		//------------------------------BOTONES-----------------------------------------------------------------------------
		
		final JButton btnA = new JButton("a");
		btnA.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnA.setBounds(189, 309, 39, 33);
		btnA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnA.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnA, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnA, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
				
			}
		});
		contentPane.add(btnA);
		
		final JButton btnB = new JButton("b");
		btnB.setFont(new Font("Tahoma", Font.PLAIN, 7));
		btnB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnB.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnB, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnB, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnB.setBounds(228, 309, 39, 33);
		contentPane.add(btnB);
		
		final JButton btnC = new JButton("c");
		btnC.setFont(new Font("Tahoma", Font.PLAIN, 7));
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnC.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnC, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnC, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnC.setBounds(267, 309, 37, 33);
		contentPane.add(btnC);
		
		final JButton btnD = new JButton("d");
		btnD.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnD.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnD, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnD, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnD.setBounds(304, 309, 39, 33);
		contentPane.add(btnD);
		
		final JButton btnE = new JButton("e");
		btnE.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnE.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnE, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnE, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnE.setBounds(343, 309, 39, 33);
		contentPane.add(btnE);
		
		final JButton btnF = new JButton("f");
		btnF.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnF.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnF, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnF, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnF.setBounds(382, 309, 37, 33);
		contentPane.add(btnF);
		
		final JButton btnG = new JButton("g");
		btnG.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnG.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnG, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnG, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnG.setBounds(420, 309, 39, 33);
		contentPane.add(btnG);
		
		final JButton btnH = new JButton("h");
		btnH.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnH.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnH, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnH, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnH.setBounds(460, 309, 39, 33);
		contentPane.add(btnH);
		
		final JButton btnI = new JButton("i");
		btnI.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnI.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnI, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnI, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnI.setBounds(499, 309, 35, 33);
		contentPane.add(btnI);
		
		final JButton btnL = new JButton("l");
		btnL.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnL.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnL, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnL, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnL.setBounds(189, 366, 35, 33);
		contentPane.add(btnL);
		
		final JButton btnM = new JButton("m");
		btnM.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnM.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnM, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnM, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnM.setBounds(226, 366, 41, 33);
		contentPane.add(btnM);
		
		final JButton btnN = new JButton("n");
		btnN.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnN.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnN, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnN, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnN.setBounds(267, 366, 39, 33);
		contentPane.add(btnN);
		
		final JButton btnJ = new JButton("j");
		btnJ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnJ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnJ.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnJ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnJ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnJ.setBounds(534, 309, 35, 33);
		contentPane.add(btnJ);
		
		final JButton btnK = new JButton("k");
		btnK.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnK.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnK, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnK, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnK.setBounds(570, 309, 37, 33);
		contentPane.add(btnK);
		
		final JButton btnO = new JButton("o");
		btnO.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnO.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnO, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnO, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnO.setBounds(304, 366, 39, 33);
		contentPane.add(btnO);
		
		final JButton btnP = new JButton("p");
		btnP.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnP.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnP, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnP, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnP.setBounds(343, 366, 39, 33);
		contentPane.add(btnP);
		
		final JButton btnQ = new JButton("q");
		btnQ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnQ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnQ.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnQ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnQ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnQ.setBounds(382, 366, 39, 33);
		contentPane.add(btnQ);
		
		final JButton btnR = new JButton("r");
		btnR.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnR.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnR, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnR, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnR.setBounds(420, 366, 37, 33);
		contentPane.add(btnR);
		
		final JButton btnS = new JButton("s");
		btnS.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnS.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnS, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnS, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnS.setBounds(460, 366, 37, 33);
		contentPane.add(btnS);
		
		final JButton btnT = new JButton("t");
		btnT.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnT.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnT, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnT, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnT.setBounds(499, 366, 37, 33);
		contentPane.add(btnT);
		
		final JButton btnU = new JButton("u");
		btnU.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnU.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnU.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnU, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnU, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnU.setBounds(534, 366, 39, 33);
		contentPane.add(btnU);
		
		final JButton btnV = new JButton("v");
		btnV.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnV.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnV, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnV, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnV.setBounds(570, 366, 39, 33);
		contentPane.add(btnV);
		
		final JButton btnW = new JButton("w");
		btnW.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnW.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnW.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnW, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnW, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnW.setBounds(189, 415, 43, 33);
		contentPane.add(btnW);
		
		final JButton btnX = new JButton("x");
		btnX.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnX.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnX, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnX, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnX.setBounds(228, 415, 39, 33);
		contentPane.add(btnX);
		
		final JButton btnY = new JButton("y");
		btnY.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnY.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnY.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnY, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnY, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnY.setBounds(267, 415, 39, 33);
		contentPane.add(btnY);
		
		final JButton btnZ = new JButton("z");
		btnZ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnZ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnZ.setEnabled(false);
				if(MetodoTeclado.metodoTeclado(btnZ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5)) {
					contadorAcierto++;
					restarVidas();
				}
				if(MetodoTeclado.metodoTeclado(btnZ, lblNewLabel1, lblNewLabel2, lblNewLabel3, lblNewLabel4 ,lblNewLabel5) == false) {
					contadorFallo--;
					restarVidas();
				}
			}
		});
		btnZ.setBounds(304, 415, 39, 33);
		contentPane.add(btnZ);
		
		lblNewLabelVidas = new JLabel("Vidas ");
		lblNewLabelVidas.setBounds(570, 11, 82, 14);
		contentPane.add(lblNewLabelVidas);
		
		//---------------------------------PALABRA A ADIVINAR----------------------------------------
		

		
		lblNewLabel1 = new JLabel(Character.toString(palabraPosibleRandom.charAt(0)));
		lblNewLabel1.setBounds(304, 135, 20, 23);
		lblNewLabel1.setBackground(Color.GRAY);
		lblNewLabel1.setForeground(Color.GRAY);
		lblNewLabel1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblNewLabel1);
		
		lblNewLabel2 = new JLabel(Character.toString(palabraPosibleRandom.charAt(1)));
		lblNewLabel2.setBounds(329, 134, 20, 25);
		lblNewLabel2.setForeground(Color.GRAY);
		lblNewLabel2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel2.setBackground(Color.BLUE);
		contentPane.add(lblNewLabel2);
		
		lblNewLabel3 = new JLabel(Character.toString(palabraPosibleRandom.charAt(2)));
		lblNewLabel3.setBounds(349, 134, 17, 25);
		lblNewLabel3.setForeground(Color.GRAY);
		lblNewLabel3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel3.setBackground(Color.BLUE);
		contentPane.add(lblNewLabel3);
		
		lblNewLabel4 = new JLabel(Character.toString(palabraPosibleRandom.charAt(3)));
		lblNewLabel4.setBounds(371, 134, 17, 25);
		lblNewLabel4.setForeground(Color.GRAY);
		lblNewLabel4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel4.setBackground(Color.BLUE);
		contentPane.add(lblNewLabel4);
		
		lblNewLabel5 = new JLabel(Character.toString(palabraPosibleRandom.charAt(4)));
		lblNewLabel5.setBounds(388, 134, 24, 25);
		lblNewLabel5.setForeground(Color.GRAY);
		lblNewLabel5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel5.setBackground(new Color(0, 0, 0));
		contentPane.add(lblNewLabel5);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(179, 299, 450, 162);
		contentPane.add(panel_1);
		

		
		

		
		//-------------------------------------------DESACTIVAR/ACTIVAR BOTONES-------------------------------------------------------
		

		
		final JButton btnDesactivarBotones = new JButton("Desactivar botones");
		btnDesactivarBotones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				desactivarActivar.desactivarActivar(btnDesactivarBotones,btnA,btnB,btnC,btnD,btnE,btnF,btnG,btnH,btnI,btnJ,btnK,btnL,
													btnM,btnN,btnO,btnP,btnQ,btnR,btnS,btnT,btnU,btnV,btnW,btnX,btnY,btnZ);
			}
		});

		btnDesactivarBotones.setBounds(267, 472, 153, 23);
		contentPane.add(btnDesactivarBotones);
		
		JButton btnNewButton = new JButton("Cambiar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(btnDesactivarBotones.getText().equalsIgnoreCase("Desactivar botones")) {
					
					btnDesactivarBotones.setText("Activar botones");
				}
				else {
					btnDesactivarBotones.setText("Desactivar botones");
				}
			}
		});
		btnNewButton.setBounds(430, 472, 104, 23);
		contentPane.add(btnNewButton);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.ORANGE);
		panel.setBounds(10, 20, 133, 413);
		contentPane.add(panel);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(559, 32, 70, 247);
		contentPane.add(panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.GRAY);
		panel_3.setForeground(Color.LIGHT_GRAY);
		panel_3.setBounds(287, 131, 133, 38);
		contentPane.add(panel_3);
		
		JLabel lblPalabraAdivinarTexto = new JLabel("Palabra de 5 letras");
		lblPalabraAdivinarTexto.setBounds(287, 109, 132, 14);
		contentPane.add(lblPalabraAdivinarTexto);
		
		JButton btnNuevaPista = new JButton("Una pista");
		btnNuevaPista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				pistas.pistas(palabraPosibleRandom);
			}
		});
		btnNuevaPista.setBounds(29, 444, 104, 23);
		contentPane.add(btnNuevaPista);
		

		JMenu MenuArchivo = new JMenu("Archivo");
		menuBar.add(MenuArchivo);
		
		JMenuItem ItemNuevaPartida = new JMenuItem("Nueva partida");
		ItemNuevaPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contadorFallo = 5;
				contadorAcierto = 0;
				numeroAleatorio = (int) (Math.random()*5);
				palabrasPosibles = new String[] {"larva", "coche", "arabe", "arena", "carta"};
				palabraPosibleRandom = palabrasPosibles[numeroAleatorio];
				lblNewLabel1.setText(Character.toString(palabraPosibleRandom.charAt(0)));
				lblNewLabel2.setText(Character.toString(palabraPosibleRandom.charAt(1)));
				lblNewLabel3.setText(Character.toString(palabraPosibleRandom.charAt(2)));
				lblNewLabel4.setText(Character.toString(palabraPosibleRandom.charAt(3)));
				lblNewLabel5.setText(Character.toString(palabraPosibleRandom.charAt(4)));
				reinicioPartida.reinicioPartida(lblNewLabel1,lblNewLabel2, lblNewLabel3,lblNewLabel4,lblNewLabel5
						,vida1,vida2,vida3,vida4,vida5,btnA,btnB,btnC,btnD,btnE,btnF,btnG,btnH,btnI,btnJ,btnK,btnL
						,btnM,btnN,btnO,btnP,btnQ,btnR,btnS,btnT,btnU,btnV,btnW,btnX,btnY,btnZ);
				cabeza.setBackground(Color.WHITE);
				Cuerpo.setBackground(Color.WHITE);
				brazoDerecho.setBackground(Color.WHITE);
				brazoIzquierdo.setBackground(Color.WHITE);
				piernaIzquierda.setBackground(Color.WHITE);
				piernaDerecha.setBackground(Color.WHITE);

				
			}
		});
		MenuArchivo.add(ItemNuevaPartida);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Salir");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmExit = JOptionPane.showConfirmDialog(null, "Estas seguro que quieres Salir?", "Confirmar", JOptionPane.YES_NO_OPTION);

                if (confirmExit == JOptionPane.YES_OPTION) {

                    System.exit(0);

                }
			}
		});
		MenuArchivo.add(mntmNewMenuItem);
		
		JMenu mnNewMenu = new JMenu("Ayuda");
		menuBar.add(mnNewMenu);
		
		JMenuItem itemSobreMi = new JMenuItem("Sobre mi");
		itemSobreMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Copyright (c) 2020, Sergi López Labrador");
			}
		});
		mnNewMenu.add(itemSobreMi);
		
		JMenuItem itemReglas = new JMenuItem("Reglas");
		itemReglas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Completa la palabra secreta antes de que se te agoten las vidas. "
                		+ "Por cada fallo, una vida menos, si no tiene vidas, pierdes. Si completas toda la palabra, ganas. "
                		+ "Hay un botón de pista que te puede ayudar a descubrir la palabra. ");
			}
		});
		mnNewMenu.add(itemReglas);

	}
	
	public static void restarVidas() {
		if (contadorFallo==4) {
			vida5.setBackground(Color.WHITE);
			cabeza.setBackground(Color.BLACK);
		}
		if (contadorFallo==3) {
			vida5.setBackground(Color.WHITE);
			vida4.setBackground(Color.WHITE);
			cabeza.setBackground(Color.BLACK);
			brazoIzquierdo.setBackground(Color.BLACK);
		}
		if (contadorFallo==2) {
			vida5.setBackground(Color.WHITE);
			vida4.setBackground(Color.WHITE);
			vida3.setBackground(Color.WHITE);
			cabeza.setBackground(Color.BLACK);
			Cuerpo.setBackground(Color.BLACK);
			brazoDerecho.setBackground(Color.BLACK);
		}
		if (contadorFallo==1) {
			vida5.setBackground(Color.WHITE);
			vida4.setBackground(Color.WHITE);
			vida3.setBackground(Color.WHITE);
			vida2.setBackground(Color.WHITE);
			cabeza.setBackground(Color.BLACK);
			Cuerpo.setBackground(Color.BLACK);
			brazoDerecho.setBackground(Color.BLACK);
			brazoIzquierdo.setBackground(Color.BLACK);
			
		}
		if (contadorFallo==0) {
			vida5.setBackground(Color.WHITE);
			vida4.setBackground(Color.WHITE);
			vida3.setBackground(Color.WHITE);
			vida2.setBackground(Color.WHITE);
			vida1.setBackground(Color.WHITE);
			cabeza.setBackground(Color.BLACK);
			Cuerpo.setBackground(Color.BLACK);
			brazoDerecho.setBackground(Color.BLACK);
			brazoIzquierdo.setBackground(Color.BLACK);
			piernaIzquierda.setBackground(Color.BLACK);
			piernaDerecha.setBackground(Color.BLACK);
			JOptionPane.showMessageDialog(null, "¡HAS PERDIDO! Para hacer otra partida ve a Archivo > Nueva partida");
		}	
		if (contadorAcierto==4) {

			JOptionPane.showMessageDialog(null, "¡HAS GANADO! Para hacer otra partida ve a Archivo > Nueva partida");
		}

	}
}
