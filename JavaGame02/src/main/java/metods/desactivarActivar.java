package metods;

import java.awt.Color;

import javax.swing.JButton;

public class desactivarActivar {
	
	public static void desactivarActivar(JButton btnDesactivarBotones,JButton btnA,JButton btnB,JButton btnC,JButton btnD,JButton 
										btnE,JButton btnF,JButton btnG,JButton btnH,JButton btnI,JButton btnJ,JButton btnK,JButton btnL
										,JButton btnM,JButton btnN,JButton btnO,JButton btnP,JButton btnQ,JButton btnR,JButton btnS
										,JButton btnT,JButton btnU,JButton btnV,JButton btnW,JButton btnX,JButton btnY,JButton btnZ) {
		
		if (btnDesactivarBotones.getText().equalsIgnoreCase("Desactivar botones")) {
			
			btnA.setEnabled(false);
			btnB.setEnabled(false);
			btnC.setEnabled(false);
			btnD.setEnabled(false);
			btnE.setEnabled(false);
			btnF.setEnabled(false);
			btnG.setEnabled(false);
			btnH.setEnabled(false);
			btnI.setEnabled(false);
			btnJ.setEnabled(false);
			btnK.setEnabled(false);
			btnL.setEnabled(false);
			btnM.setEnabled(false);
			btnN.setEnabled(false);
			btnO.setEnabled(false);
			btnP.setEnabled(false);
			btnQ.setEnabled(false);
			btnR.setEnabled(false);
			btnS.setEnabled(false);
			btnT.setEnabled(false);
			btnU.setEnabled(false);
			btnV.setEnabled(false);
			btnW.setEnabled(false);
			btnX.setEnabled(false);
			btnY.setEnabled(false);
			btnZ.setEnabled(false);
			
		} else {
			
			btnA.setEnabled(true);
			btnB.setEnabled(true);
			btnC.setEnabled(true);
			btnD.setEnabled(true);
			btnE.setEnabled(true);
			btnF.setEnabled(true);
			btnG.setEnabled(true);
			btnH.setEnabled(true);
			btnI.setEnabled(true);
			btnJ.setEnabled(true);
			btnK.setEnabled(true);
			btnL.setEnabled(true);
			btnM.setEnabled(true);
			btnN.setEnabled(true);
			btnO.setEnabled(true);
			btnP.setEnabled(true);
			btnQ.setEnabled(true);
			btnR.setEnabled(true);
			btnS.setEnabled(true);
			btnT.setEnabled(true);
			btnU.setEnabled(true);
			btnV.setEnabled(true);
			btnW.setEnabled(true);
			btnX.setEnabled(true);
			btnY.setEnabled(true);
			btnZ.setEnabled(true);
			
		}
		
	}
}
	
	


