package metods;

import javax.swing.JOptionPane;

public class pistas {
	
	public static void pistas(String palabraPosibleRandom) {
		
		if(palabraPosibleRandom.contentEquals("larva")) {
			JOptionPane.showMessageDialog(null, "Animal con desarrollo indirecto (con metamorfosis)");
		}
		if(palabraPosibleRandom.contentEquals("coche")) {
			JOptionPane.showMessageDialog(null, "Va por la carretera");
		}
		if(palabraPosibleRandom.contentEquals("arabe")) {
			JOptionPane.showMessageDialog(null, "Es una macrolengua de la familia semítica");
		}
		if(palabraPosibleRandom.contentEquals("arena")) {
			JOptionPane.showMessageDialog(null, "Conjunto de fragmentos sueltos de rocas o minerales de pequeño tamano");
		}
		if(palabraPosibleRandom.contentEquals("carta")) {
			JOptionPane.showMessageDialog(null, "Es un medio de comunicación escrita");
		}
		
	}

}
